#Forkio Step Project

### _Members:_
 * Max Bilyk | **_`Slack: Max Bilyk`_**
 * Viktoria Voikshnaras | **_`Slack: Viktoriya`_**
 
### _Used technologies:_
     * BEM
     * Gulp
     * Gulp plugins (gulp-sass, gulp-imagemin etc.)
     * Sass/SCSS preproccesors
     * Jquery
     * NPM
     * Responsive layout with breakpoints   

### _Work done by students:_
* Max Bilyk:
    * Revolution Editor Section
    * Here is what you get Section
    * Pricing Subscription Section
    * Creating `README.md` file
    * Set up npm packages
* Viktoria Voikshnaras:
    * Header and _Fork App_ Section
    * Adaptive menu _' Burger '_
    * Talking about Fork Section
    * Downloaded pictures from the layout and inserted into the project
    * Published the project on the Internet using github/gitlab pages 
* The work done together:
    * Creating Git Lab Repository
    * Set up `gulpfile.js` file 
    * And set up some starting folders for the project
 