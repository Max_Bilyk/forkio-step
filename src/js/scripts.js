$(document).ready(function () {
    $('.hat-header__burger').click(function (event) {
        $('.hat-header__burger,.hat-header__menu').toggleClass('active');
    });

    $('a.menu-link').click(function (event) {
        $(' a.menu-link').toggleClass('active');
    });

    $('.hat-header__logo').click(function (event) {
        $('.logo-img, .logo-txt').toggleClass('active');
    });

});